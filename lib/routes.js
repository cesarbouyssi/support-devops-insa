exports.order = function order(req, res, next) {
  // TODO implement from here
  var countries = {"DE" : 0.20, "UK" : 0.21,"FR":0.20, "IT":0.25, "ES":0.19,"PL":0.21,"RO":0.20,"NL":0.20,"BE":0.24,"EL":0.20, "CZ":0.19, "PT":0.23,"HU":0.27,"SE":0.23, "AT":0.22, "BG":0.21, "DK":0.21, "FI":0.17, "SK":0.18, "IE":0.21, "HR":0.23 , "LT":0.23, "SI":0.24, "LV":0.20, "EE":0.22, "CY":0.21, "LU":0.25 , "MT": 0.20}

  try{
    quantity = req.body.quantities
    country = req.body.country
    prices = req.body.prices
    reductionType = req.body.reduction
    console.log("request --> \n ", req.body)
    //
    // if(typeof(reductionType) !== "string"){
    //   throw new Error(" blabla")
    // }
    //
    // if(!checkCountry(country, countries)){
    //   throw new Error("country")
    // }
    //
    // if(!checkQuantity(quantity)){
    //   throw new Error("quantity")
    // }
    //
    // if(prices.length != quantity.length){
    //   throw new Error("tab length")
    // }

    taxe = countries[country]

    totalHT = getPriceHT(quantity, prices)
    totalTTC = getPriceTTC(totalHT, taxe, country)
    totalDiscount = applyDiscount(totalTTC, reductionType, country)
    console.log("totalDiscount --> \n ", totalDiscount)
    res.json({"total":totalDiscount});
    //res.json({});
  }catch(e){
    //console.log(e)
    res.status(400).end()
  }


  //res.json({"total":totalDiscount});
}

exports.feedback = function feedback(req, res, next) {
  console.info("FEEDBACK:", req.body.type, req.body.content);
  next();
}

function getPriceHT(nbArticles, price) {
  total = 0

  for(var i = 0; i<nbArticles.length; i++ ){
    total+= nbArticles[i] * price[i]
  }
   return total;
}

function getPriceTTC(total, taxe, country){
  /*if(country === "SK"){
    if(total > 2000){
      return total
    }else{
      return total*(1.18)
    }
  }else if(country === "BE"){
    if(total <= 2000){
      return total
    }else{
      return total*1.25
    }
  }else if(country === "UK"){
    return total * 0.75
  }
  else{*/
    return total * (taxe+1);
  //}
}

function applyDiscount(price, reductionType, country){
  if(reductionType === "STANDARD" ){
    if(price > 50000){
      return price - price*0.15
    }else if(price > 10000){
      return price - price*0.10
    }else if(price > 7000){
      return price - price*0.07
    }else if(price > 5000){
      return price - price*0.05
    }else if(price > 1000){
      return price - price*0.03
    }else{
      return price
    }
  }else if(reductionType === "HALF PRICE"){
    return price*0.5
  }else{
    return price
  }
}

function checkCountry(country, countries){
  if(country ){
    return false
  }else{
    return true
  }
}

function checkQuantity(quantity){
  console.log("type pf",typeof(quantity))
  if(typeof(quantity) === "object"){
    for(var i = 0; i < quantity.lenght ; i++){
      if(!typeof(quantity[i]) === "number"){
        return false
      }
    }
    return true
  }else{
    return false
  }
}
